#vimconfig
The install process is rather simple.
Just do:
```
git clone https://IGI-111@bitbucket.org/IGI-111/vimconfig.git /tmp/vimconfig
mv /tmp/vimconfig/{.vimrc,.vim} ~
rm -rf /tmp/vimconfig
git clone https://github.com/Shougo/neobundle.vim ~/.vim/bundle/neobundle.vim
```
Then, launch vim and use vundle to install the plugins with `:NeoBundleInstall`.
